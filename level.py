# Level related (main data model)

from .entity import *
from .query import Query
from collections import defaultdict
import itertools

class CellList:
	def __init__(self):
		self.cell_entities = []

	def _trace(self, msg, e):
		# print(msg, e, e.x, e.y, next(t[0] for t in self._level._matrix.items() if t[1]==self), self.cell_entities)
		pass

	def _append(self, e):
		self._trace('APPEND', e)
		assert isinstance(e, Entity)
		self.cell_entities.append(e)
		self._trace('APPENDED', e)

	def _remove(self, e):
		self._trace('REMOVE', e)
		assert e in self.cell_entities
		self.cell_entities.remove(e)
		self._trace('REMOVED', e)

	# def __iter__(self):
	# 	return iter(self.cell_entities)
	#
	# def __bool__(self):
	# 	return bool(self.cell_entities)
	#
	# def matching(self, **kwargs):
	# 	return [e for e in self.cell_entities if set(kwargs.items()).issubset(set(e.items()))]
	#
	# def where(self, func):
	# 	return list(filter(func, self.cell_entities))
	#
	# def has(self, **kwargs):
	# 	# if __debug__:
	# 	# 	assert 'type' not in kwargs or kwargs['type'] in self._level.by_type
	# 	return any(self.matching(**kwargs))

class Level:
	def __init__(self, width, height):
		self.map_w = width
		self.map_h = height
		self._matrix = defaultdict(self._cell_factory)	# Entity heap matrix, for rendering and data queries
		self.entities = []								# All entities in one big bag
		#self.dirty = set()								# Dirty positions for the renderer to update
		self.step_moves = {}							# Entity : (x, y)
		self.by_type = defaultdict(list)				# Entity.type : Entity

		self.dirty = set(itertools.product(range(width), range(height)))

	def add(self, e):
		assert e not in self.entities, 'Adding already present entity from level (did you forget to .clone()?)'
		assert 0<=e.x<self.map_w and 0<=e.y<self.map_h, 'Adding entity outside level bounds'
		assert e.type is not None, 'Level entities must have a .type'
		assert not isinstance(e.type, Entity), 'Did you set a prefab as Entity type?'
		e.level = self
		newp = e.x, e.y
		self.entities.append(e)
		self.by_type[e.type].append(e)
		self._matrix[newp]._append(e)
		self.dirty.add(newp)

	def remove(self, e):
		assert e in self.entities, 'Removing non present entity from level'
		rem_p = e.x, e.y
		e.level = None
		self.dirty.add(rem_p)
		self._matrix[rem_p]._remove(e)
		self.entities.remove(e)
		self.by_type[e.type].remove(e)

	def query_cell(self, x, y) -> Query:
		return Query(self._matrix[x, y].cell_entities)

	def query_all(self):
		return Query(self.entities)

	def query_nearby(self, x, y, distance, diamond=False, include_center=True) -> Query:
		""" Query nearby items for position x, y.
		:param diamond: If true, it returns diamond "Von Neumann neighbourhood", otherwise a box "Moore neighbourhood"
		"""
		assert distance >= 0, 'Query nearby needs positive or zero distance'
		# https://en.wikipedia.org/wiki/Von_Neumann_neighborhood
		# https://en.wikipedia.org/wiki/Moore_neighborhood
		return self.query_box(x, y
							  , 1+distance*2, 1+distance*2
							  , include_center
							  , lambda bx, by: (bx-x)+(by-y)<=distance if diamond else None)

	def query_box(self, x, y, box_width, box_height, include_center=True, pos_predicate=None) -> Query:
		"""
		Query a box centered at x, y. Width/height should be odd for unbiased vicinity.
		If pos_predicate is not None takes (x, y) and returns true to keep them.
		"""
		assert box_width > 0 or box_height > 0, 'box parameters should be positive'
		assert box_width%2 == 1 or box_height%2 == 1, 'box parameters should be odd or nearby would return biased vicinity'
		hw = (box_width-1)//2
		hh = (box_height-1)//2
		range()
		poss = itertools.product(range(x-hw, x+hw+1), range(y-hh, y+hh+1))
		if not include_center:
			c = (x, y)
			poss = filter(lambda p: p != c, poss)
		if pos_predicate:
			poss = filter(pos_predicate, poss)
		return Query(itertools.chain.from_iterable(self._matrix[p] for p in poss))

	def coexisting_with(self, e) -> Query:
		assert isinstance(e, Entity), 'e is not an Entity'
		if not e.level:			# Entity is probably dead
			print('Entity {} not in level'.format(e))
			return Query(())
		else:
			return self.query_cell(e.x, e.y)

	def post_step(self):
		# This avoids temporary position (move x -> move y) from rendering
		for e, oldp in self.step_moves.items():
			newp = e.x, e.y
			self.dirty.add(newp)
			self.dirty.add(oldp)
		self.step_moves.clear()

	def _changing(self, e, key, old, new):
		""" called when about to change an Entity variable (before, so 'e.key' has the old value still) """
		assert e in self.entities

		if old == new:
			return

		cur_e = e.x, e.y

		if key == 'type':
			self.by_type[old].remove(e)
			self.by_type[new].append(e)

		elif key == 'x':
			# Yes, this will probably cause two+ moves per step but, as movements are executed sequentially,
			# constraints need to have up-to-date data, and we play on the safe side (re-iteration of entities)
			print(cur_e, key, old, new, e.__dict__)
			try:
				self._matrix[cur_e]._remove(e)
				self._matrix[new, cur_e[1]]._append(e)
			except:
				for k, v in self._matrix.items():
					if e in v.cell_entities:
						print('found in ', k=True)
				raise

		elif key == 'y':
			print(cur_e, key, old, new, e.__dict__)
			try:
				self._matrix[cur_e]._remove(e)
				self._matrix[cur_e[0], new]._append(e)
			except:
				for k, v in self._matrix.items():
					if e in v.cell_entities:
						print('found in ', k, flush=True)
				raise

		# Anything that's changed, just save entity and old position (changed for key 'x' or 'y')
		if e not in self.step_moves:
			# First touch, cache old position to just update once (later) and not for every change
			self.step_moves[e] = cur_e

	def _cell_factory(self):
		cl = CellList()
		if __debug__:
			cl._level = self
		return cl

	def __repr__(self):
		return 'LEVEL @{:x}'.format(id(self))


def load_level(file, char_to_prototype_map):
	with open(file, 'r') as f:
		lines = [line for line in (orig_line.split('--')[0].rstrip('\n\r') for orig_line in f.readlines()) if line]
		width = max(len(line) for line in lines)
		height = len(lines)
		lv = Level(width, height)
		for y, line in enumerate(lines):
			for x, char in enumerate(line):
				proto = char_to_prototype_map.get(char)
				if proto:
					lv.add(proto.clone(x=x, y=y))
		return lv

