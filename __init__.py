__all__ = ['game', 'level', 'curses_io', 'CursesPuzzle', 'pathfinding']

from . import game, curses_io, level
import curses

class CursesPuzzle:
	input_move_map = {
		curses.KEY_LEFT: (-1, 0)
		, curses.KEY_RIGHT: (1, 0)
		, curses.KEY_UP: (0, -1)
		, curses.KEY_DOWN: (0, 1)

		, ord('a'): (-1, 0)
		, ord('d'): (1, 0)
		, ord('w'): (0, -1)
		, ord('s'): (0, 1)
	}

	def __init__(self, level_path, curses_main_win=None):
		curses_main_win = curses_main_win or curses_io.stdscr
		max_y, max_x = curses_main_win.getmaxyx()
		self.level_path = level_path
		#self.user_stop = False
		self.reset()
		self.win_board = curses_io.new_window(width=self.level.map_w, height=self.level.map_h, x=2, y=4, derive_from=curses_main_win)
		self.win_msg = curses_io.new_window(width=max_x - 2, height=2, x=0, y=max_y - 4, boxed=True, derive_from=curses_main_win)

	def render(self):
		curses_io.render(self.win_board, self.level, game.render_info)

	def show_status(self, status):
		curses_io.show_message(self.win_msg, 'ESC or Q keys to quit, R to reset, others to advance one turn\n' + status)

	def reset(self):
		#self.user_stop = False
		if self.level_path:
			self.level = level.load_level(self.level_path, game.char_to_prototype)
			self.player = next(x for x in self.level.entities if getattr(x, 'is_player', False))
			assert self.player, "No player '.' in " + self.level_path
		else:
			self.level, self.player = game.random_level()

	@staticmethod
	def process_input():
		k = curses_io.getch()
		# just_pressed_repr = str(chr(k) if 32 < k < 255 else k))
		if k == 27 or k == ord('q'):	# 27 is ESCAPE
			return (game.QUIT,)
		elif k == ord('r'):
			return (game.RESET,)
		else:
			player_rmove = CursesPuzzle.input_move_map.get(k)
			if player_rmove:
				return game.RELATIVE_MOVE, player_rmove
		return (None,)

	def step(self, extra_moves):
		game.step(self.level, extra_moves)

	# def close(self):
	#
	# def __enter__(self):
	# 	return self
	#
	# def __exit__(self, exc_type, exc_val, exc_tb):
	# 	self.close()
