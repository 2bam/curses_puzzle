-- In this level Comma should assist Point with the ampersand-bell
-- And then Point could open the door such that Comma gets the cheese (but will get eventually eaten)
      A  #            A
    >    #
         #
####0#####    .
         #
        &#
     ,   #
         #            @
