# curses_puzzle

Puzzle simulation test using curses python lib. It's also a package.

It has grown a bit in order to make it more compatible for training neural networks.
For the simplest most succinct version look for the `pre-command-defer` tagged commit at `master` branch.

Only tested on Windows 7 64-bit, Python 3.6.3.
For windows install windows-curses using pip
```pip install windows-curses```

To run it
```py -3.6 example.py```
or
```python3.6 example.py```

# Structure

### entity.py

`Entity` is the simulation unit having:

* A custom `type`
* 2D cartesian coordinates `x` and `y`
* `clone()` capability for prototype pattern _(patching with parameters, e.g. Enemy.clone(x=10, y=5))_
* a `level` reference to notify on attribute changes _(to fix structure caches and record dirtiness for renderer)_
* any other attribute one might want to add dynamically _(e.g. is_open, health...)_

### level.py
`Level` is a "matrix of lists". A 2D board with multiple entities on each cell.

* You can `add()`, and `remove()` entities.
* You can access all added `entities` list or `by_type`.
* Or the `matrix` of the level, which is formed by `CellList`s.
* `coexists_with()` helper gets the entity's `CellList` for it's position.

`CellList` are entity lists you can comfortably query:

* `filter()` its contents by attributes (e.g. `cell.filter(type=MOUSE)`)
* check if it `has()` any elements that match a attributes (e.g. `cell.has(type=Door, is_open=False)`)

### curses_io.py

Has utilities to get input, make curses windows and render levels via `render()`

### game.py

It's the main game file, has all the processing in `step()`, and constraint examples in `can_move()`.

### __init__.py

`CursesPuzzle` is an encapsulation of game and curses i/o interfacing ready to use.

# Package

You can use it as a package for your own simulation needs.