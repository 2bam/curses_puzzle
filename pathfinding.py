from collections import deque

def expand_checkerboard(curr_point):
	"""
	Expands in 4-direction diamond with checkerboard mechanism:
	Prioritize X over Y if on a white cell (even-even or odd-odd: x%2 != y%2) and viceversa on black.
	"""
	# NOTE: The following breaks sequentiality that makes cats follow player if moving horizontally,
	# 		but only approach if players moves vertically! As cats have no state and nearest target could change per frame
	#		this is a nice heuristic solution
	if curr_point[0] % 2 == curr_point[1] % 2:
		dirs = ((-1, 0), (1, 0), (0, -1), (0, 1))
	else:
		dirs = ((0, -1), (0, 1), (-1, 0), (1, 0))
	return dirs

def expand_von_neumann(curr_point):
	""" Expand in 4-direction diamond. """
	return ((-1, 0), (1, 0), (0, -1), (0, 1))

def expand_moore(curr_point):
	""" Expand in 8-direction box. """
	return (  (-1, -1), (-1, 0), (-1, 1)
			, ( 0, -1),          ( 0, 1)
			, ( 1, -1), ( 1, 0), ( 1, 1)
			)

def shortest_path_with_fallbacks(level, src, expand, is_blocked, is_goal, *goal_fallbacks):
	"""
	Find the shortest path in a level, highly customizable by lambdas.
	:param level: Level to do the search in
	:param src: Source position (x, y) tuple
	:param expand: Expand function, takes an absolute position tuple, should return an iterable of direction tuples (relative positions)
	:param is_blocked: Predicate which takes an absolute position and determines if the previous expansion is valid.
	:param is_goal: Main goal predicate function, takes a Query. If returns True, finding is complete and the path is returned.
	:param goal_fallbacks: Fallbacks, each take a Query. Similar to is_goal, results are cached in case is_goal always returns False.
	:return: Can return (None, -1) if not found, (path, goal_index) if goal(0) or fallback(1+) are found.
	"""
	fallback_found = [None]*len(goal_fallbacks)
	visited = {src}
	prev = {src: None}
	q = deque([(src, level.query_cell(*src))])
	steps = 0

	def build_path(point, fallback_index):
		path = []
		path_point = point
		while path_point:
			path.append(path_point)
			path_point = prev[path_point]
		path.reverse()
		assert path[0] == src
		print('shortest_path found, steps', steps, 'of max ', level.map_w * level.map_h, 'fallback:'+str(fallback_index) if fallback_index else '')
		return path, fallback_index

	while q:
		steps += 1
		curr_point, curr_cell = q.popleft()

		#cell = level.query_cell(*curr_point)
		if is_goal(curr_cell):
			return build_path(curr_point, 0)
		for i, fallback in enumerate(goal_fallbacks):
			if fallback_found[i]:
				break		# Already found a good fallback... no need to keep looking for lesser ones
			else:
				if fallback(curr_cell):
					fallback_found[i] = curr_point
					break

		directions = expand(curr_point)
		neighbours = [(curr_point[0]+rx, curr_point[1]+ry) for rx, ry in directions]
		for npos in neighbours:
			if npos in visited:
				continue

			visited.add(npos)

			if not (0 <= npos[0] < level.map_w and 0 <= npos[1] < level.map_h):
				continue

			# NOTE: Is good to have blocked separate from expand, it avoids calling the is_blocked predicate
			# 		multiple times for the same position.
			ncell = level.query_cell(*npos)
			if not is_blocked(ncell):
				prev[npos] = curr_point
				q.append((npos, ncell))

	for fallback_index, fallback_point in enumerate(fallback_found, start=1):
		if fallback_point:
			return build_path(fallback_point, fallback_index)

	print('shortest_path not found, steps', steps, 'of max', level.map_w*level.map_h)
	return None, -1
