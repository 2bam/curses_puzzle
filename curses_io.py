import curses

stdscr = None
_title = None
max_xy = (0, 0)
_msgwin = None

def init(title = None, message = None):
	global _title, stdscr, max_xy, _msgwin
	_title = title or 'Curses puzzle demo'

	stdscr = curses.initscr()
	stdscr.keypad(1)  # Special key translations
	curses.noecho()
	curses.cbreak()
	curses.curs_set(0)  # No cursor
	my, mx = stdscr.getmaxyx()
	max_xy = mx, my
	# max_xy = (min(80, mx), min(25, my))	# min-hack
	try:
		curses.start_color()
	except:
		pass

	stdscr.erase()
	stdscr.addstr(1, 1, _title)
	stdscr.refresh()

	print('terminal max xy', mx, my)

def show_message(msgwin, txt, off_x=0, off_y=0):
	assert off_x>=0 and off_y>=0
	max_y, max_x = msgwin.getmaxyx()
	msgwin.erase()
	for i, line in enumerate(txt.splitlines(), 0):
		x = off_x
		y = i+off_y
		if y >= max_y:
			break
		try:
			msgwin.addstr(y, x, line[:max_x-off_x])
		except curses.error:
			# Attempting to write to the lower right corner of a window, subwindow, or pad will cause an exception to be raised after the character is printed.
			# https://docs.python.org/3/library/curses.html#curses.window.addch
			pass
	msgwin.refresh()

def new_window(width, height, x, y, boxed=True, derive_from=None):
	derive_from = derive_from or stdscr
	if boxed:
		box = derive_from.derwin(height+2, width+2, y, x)
		box.box()
		box.refresh()
		win = box.derwin(height, width, 1, 1)
	else:
		win = derive_from.derwin(height, width, y, x)
	stdscr.refresh()

	return win
	
def getch():
	return stdscr.getch()


# Show model on curses
def render(win, level, render_info):
	def _priority_key(entity):
		p = render_info[entity.type]['show_priority']
		if callable(p):
			p = p(entity)
		return p, entity.type

	# Update only dirty parts
	for coord in level.dirty:
		cell_ents = level.query_cell(*coord)
		if not cell_ents:
			character = ' ' if coord[0]%2 == coord[1]%2 else '·'	# Checkerboard
		else:
			top = min(cell_ents, key=_priority_key)
			ri = render_info[top.type]
			renderer = ri.get('renderer')
			character = renderer(top) if renderer else ri['character']
		#print(coord, character, ord(character))
		try:
			win.addch(coord[1], coord[0], character)
		except curses.error:
			# Attempting to write to the lower right corner of a window, subwindow, or pad will cause an exception to be raised after the character is printed.
			# https://docs.python.org/3/library/curses.html#curses.window.addch
			pass

	level.dirty.clear()
	win.refresh()

def close():
	# Set everything back to normal
	stdscr.keypad(0)
	curses.echo()
	curses.nocbreak()
	curses.endwin()
