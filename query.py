
import itertools
import collections
class Query:
	""" Chainable query for convenience. It caches results so it can be called and iterated multiple times. """

	def __init__(self, it):
		self._l = list(it)

	def __iter__(self):
		return iter(self._l)

	def __bool__(self):
		return bool(self._l)

	@staticmethod
	def chain(*queries):
		# Removes repeated ones, but keeps order
		od = collections.OrderedDict.fromkeys(itertools.chain.from_iterable(queries))
		return Query(od.keys())

	def matching(self, **kwargs):
		return Query(e for e in self._l if set(kwargs.items()).issubset(set(e.items())))

	def where(self, func):
		return Query(filter(func, self._l))

	def has(self, **kwargs):
		# if __debug__:
		# 	assert 'type' not in kwargs or kwargs['type'] in self._level.by_type
		return any(self.matching(**kwargs))

	def __repr__(self):
		return '[QUERY '+str(self._l)[1:]

