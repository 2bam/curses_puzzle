# Entity

import collections

# Entity notifies attribute changes to level (if not None)
class Entity(collections.MutableMapping):
	__default_attribs__ = {'x': 0, 'y': 0, 'level': None, 'type': None}

	def clone(self, **new_attribs):
		"""Shallow clone"""
		assert self.level is None, "Can't clone an entity integrated to a level. Error-prone, remove then re-add both if needed."

		# Python 3.6.3 bug workaround: Even being Entity a mapping raises a
		# 							   TypeError: ABCMeta object argument after ** must be a mapping
		e = Entity(**{**self})

		e.update(new_attribs)			# Can't return {**a, **b} because of repeated keys
		return e

	def __init__(self, **kwargs):
		object.__setattr__(self, 'level', None)	# Manual set, because __setattr__ uses it.
		self.update(Entity.__default_attribs__)
		self.update(kwargs)

	# Helper by-string accessor (avoid get/set/delattr calls on entity)
	def __getitem__(self, key):
		return getattr(self, key)

	def __setitem__(self, key, value):
		setattr(self, key, value)

	def __delitem__(self, key):
		delattr(self, key)

	def __setattr__(self, key, value):
		level = self.level
		if level:
			level._changing(self, key, getattr(self, key), value)
		object.__setattr__(self, key, value)

	def __iter__(self):
		return iter(self.__dict__)

	def __len__(self):
		return len(self.__dict__)

	def __repr__(self):
		unique_attribs = ('{}={}'.format(k, v) for k, v in self.__dict__.items() if k not in Entity.__default_attribs__)
		return '[E:{} ({},{}) @0x{:x} {}]'.format(
			self.type or 'UNK'
			, self.x
			, self.y
			, id(self)
			, ''.join(unique_attribs)
		)

	def __hash__(self):
		return id(self)

	def __eq__(self, other):
		return id(self) == id(other)
