# Puzzle simulation test using curses python lib.
# NOTE: For windows install windows-curses via pip

import random
from .entity import *
from .level import *
from .more_math import *
from . import curses_io
from collections import deque
import math

from . import pathfinding

# Some meanings for keys bindings
QUIT = 'QUIT'
RESET = 'RESET'
RELATIVE_MOVE = 'RELATIVE_MOVE'

# Define types
CHEESE = 'CHEESE'
CAT = 'CAT'
MOUSE = 'MOUSE'
TRIGGER = 'TRIGGER'
DOOR = 'DOOR'
WALL = 'WALL'
BELL = 'BELL'

# Define prefabs
Cheese = Entity(type=CHEESE)
Cat = Entity(type=CAT, is_idle=True)
Mouse = Entity(type=MOUSE)
Trigger = Entity(type=TRIGGER, id=0)
Door = Entity(type=DOOR, is_open=False, id=0)
Wall = Entity(type=WALL)
Bell = Entity(type=BELL)

# Define rendering info.
render_info = {
	CHEESE: {'show_priority': 1, 'character': '>'}
	, CAT: {'show_priority': 3, 'character': '@'}
	, MOUSE: {'show_priority': lambda mouse: 4 if mouse.is_player else 5, 'renderer': lambda mouse: '.' if mouse.is_player else ','}
	, TRIGGER: {'show_priority': 6, 'character': 'X'}
	, DOOR: {'show_priority': 7, 'renderer': lambda door: '_' if door.is_open else '/'}
	, WALL: {'show_priority': 8, 'character': '#'}
	, BELL: {'show_priority': 9, 'character': '&'}
}

# Define level loading info.
char_to_prototype = {
	'#': Wall
	, '>': Cheese
	, '@': Cat
	, '.': Mouse.clone(is_player=True)
	, ',': Mouse.clone(is_player=False)
	, 'A': Trigger.clone(id=0)
	, 'B': Trigger.clone(id=1)
	, 'C': Trigger.clone(id=2)
	, '0': Door.clone(id=0)
	, '1': Door.clone(id=1)
	, '2': Door.clone(id=2)
	, '&': Bell
}



# Constraints
def can_move(level, e, to_x, to_y):
	# Boundaries constraint
	if not (0 <= to_x < level.map_w and 0 <= to_y < level.map_h):
		return False

	# Don't allow two cats in the same place or mice going to their certain death
	# Also don't allow mice and cats going through closed doors
	to_cell = level.query_cell(to_x, to_y)
	if e.type in (CAT, MOUSE) and (to_cell.has(type=CAT) or to_cell.has(type=WALL) or to_cell.has(type=DOOR, is_open=False)):
		return False

	return True


# Take a turn in the "game"
def step(level, extra_moves):
	# Cached lists references (important to be refs in case they get inter-step removals by 'level')
	cats = level.by_type[CAT]
	mice = level.by_type[MOUSE]
	triggers = level.by_type[TRIGGER]
	doors = level.by_type[DOOR]
	bells = level.by_type[BELL]


	for e, p in extra_moves:
		if can_move(level, e, *p):
			e.x, e.y = p


	# Mice move randomly (drunk walking)
	# for mouse in mice:
	# 	to_x = mouse.x + random.randint(-1, 1)
	# 	to_y = mouse.y + random.randint(-1, 1)
	# 	if can_move(level, mouse, to_x, to_y):
	# 		mouse.x = to_x
	# 		mouse.y = to_y


	#touched_bells = [bell for bell in bells if level.coexisting_with(bell).has(type=MOUSE)]
	for cat in cats:
		# Cats follow closest ringing bell or mice (if any), in that order

		nearest_path, fallback_index = pathfinding.shortest_path_with_fallbacks(
			level
			, (cat.x, cat.y)
			, pathfinding.expand_checkerboard
			# Is blocked?
			, lambda cell: any(cell.where(lambda e: e.type == WALL or e.type == DOOR and not e.is_open))
			# Goal predicate (and fallback)
			, lambda cell: cell.has(type=MOUSE) and cell.has(type=BELL)
			, lambda cell: cell.has(type=MOUSE)
			)

		if not nearest_path or len(nearest_path) <= 1:	# Path len 1 means we are already at destination.
			cat.is_idle = True
		else:
			cat.is_idle = False
			to_x, to_y = nearest_path[1]
			if can_move(level, cat, to_x, to_y):
				cat.x = to_x
				cat.y = to_y

		# Kill all mice under cats
		for eaten in level.coexisting_with(cat).matching(type=MOUSE):
			level.remove(eaten)

	# Triggers get pressed by cats, any trigger pressed will open all doors with same id
	triggered = set()
	for trigger in triggers:
		if any(level.coexisting_with(trigger).where(lambda e: e.type in (CAT, MOUSE))):
			triggered.add(trigger.id)

	for door in doors:
		set_open = door.id in triggered
		if door.is_open != set_open:
			door.is_open = set_open

	# Do cleanup (queued removals, etc.)
	level.post_step()





# Place stuff randomly (not repeating places)
def init_cells_randomly(level, stuff):
	free_cells = list(filter(lambda p: not level.query_cell(*p), itertools.product(range(level.map_w), range(level.map_h))))

	random.shuffle(free_cells)
	for prefab, amt in stuff.items():
		for _ in range(amt):
			if not free_cells:
				return
			cell_x, cell_y = free_cells.pop()
			level.add(prefab.clone(x=cell_x, y=cell_y))


def random_level():
	# Create the level with given dimensions
	level = Level(20, 10)

	# Define amount of stuff to add
	stuff = {
		Cat: 1
		, Mouse.clone(is_player=False): 2
		, Trigger: 3
		, Bell: 1
	}

	def create_wall(x, y, h, v, l):
		for i in range(l):
			if not (0<=x+i*h<level.map_w and 0<=y+i*v<level.map_h):
				break
			level.add(Wall.clone(x=x+i*h, y=y+i*v))

	r = random.randrange(1, min(level.map_h, level.map_w)-1)
	wx, wy, v = random.choice([(r,0,1), (0,r,0)])
	h = 1-v
	for _ in range(4):
		l = random.randrange(3, 6)
		create_wall(wx, wy, h, v, l)
		wx += h*l
		wy += v*l
		if not v:
			h = 0
			v = random.choice([-1, 1])
		else:
			h = random.choice([-1, 1])
			v = 0

	for _ in range(2):
		if level.by_type[WALL]:
			dp = random.choice(level.by_type[WALL])
			level.add(Door.clone(x=dp.x, y=dp.y))
			level.remove(dp)

	# Initialize with random stuff
	init_cells_randomly(level, stuff)

	pl_x, pl_y = random.choice(list(filter(lambda p: not level.query_cell(*p), itertools.product(range(level.map_w), range(level.map_h)))))
	player = Mouse.clone(x=pl_x, y=pl_y, is_player=True)
	level.add(player)
	return (level, player)

#wrapper(main)







