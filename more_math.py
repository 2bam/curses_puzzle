# Math utilities

def clamp(x, lo, hi):
	return min(max(lo, x), hi)


def sign(x):
	if x == 0: return 0
	elif x < 0: return -1
	else: return 1
