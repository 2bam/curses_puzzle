# Usage example

# Hacky stuff for non-package relative imports (package compatible)
# You should call python with arguments "-m curses_puzzle.example"
#	but this will make it work just by running the example.py file directly.
#
# Read more:
#	https://alex.dzyoba.com/blog/python-import/ (Excellent article)
#	https://stackoverflow.com/questions/14132789/relative-imports-for-the-billionth-time
#	https://stackoverflow.com/a/6466139/10060021
#	https://www.python.org/dev/peps/pep-0366/
# 	https://docs.python.org/3/reference/import.html
if __name__ == '__main__' and __package__ is None:
	import sys, os
	file_dir = os.path.dirname(os.path.abspath(__file__))
	sys.path.insert(0, os.path.dirname(file_dir))
	__package__ = os.path.basename(file_dir)
	if __package__ != 'curses_puzzle':
		print('The parent directory should be named curses_puzzle! @', __file__)
# End of hacky stuff


from . import CursesPuzzle, curses_io
from .game import *

curses_io.init()

# puzzle = CursesPuzzle('levels/level-HOWTO.txt')
puzzle = CursesPuzzle(None)		# Random generation.

user_stop = False
while not user_stop:
	# Update game status
	num_idle_cats = sum(1 for cat in puzzle.level.by_type[CAT] if cat.is_idle)
	status = ''
	if puzzle.level.coexisting_with(puzzle.player).has(type=BELL):		# Even if player is not in level, it should work.
		status += ' RING!!!'
	if num_idle_cats:
		status += ' CATS TRAPPED OR LONELY: ' + str(num_idle_cats)
	puzzle.show_status(status)

	# Render
	puzzle.render()

	# Get processed player input
	player_rmove = None
	input = puzzle.process_input()
	action = input[0]
	if action == QUIT:
		user_stop = True
		break
	elif action == RESET:
		puzzle.reset()
		continue
	elif action == RELATIVE_MOVE:
		player_rmove = input[1]

	# Add extra moves for the game's step
	extra_moves = []

	if player_rmove:
		player_move = puzzle.player.x + player_rmove[0], puzzle.player.y + player_rmove[1]
		extra_moves.append((puzzle.player, player_move))

	# First non-player mouse will move to the left perpetually
	non_player_mouse = next((x for x in puzzle.level.entities if x.type == MOUSE and not x.is_player), None)
	if non_player_mouse:
		extra_moves.append((non_player_mouse, (non_player_mouse.x - 1, non_player_mouse.y)))

	puzzle.step(extra_moves)

	# puzzle.close()

curses_io.close()
